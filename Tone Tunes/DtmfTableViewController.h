//
//  DtmfTableViewController.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tune.h"
#import "DtmfPadViewController.h"
#import "MainScreenViewController.h"

@interface DtmfTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *dtmfList;
@property (nonatomic, strong) NSArray *dtmfKeys;
@property (nonatomic, strong) NSArray *dtmfSongs;

@end
