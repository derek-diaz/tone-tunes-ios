//
//  SamsungTableViewController.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "Tune.h"
#import "MainScreenViewController.h"
#import "SamsungPadViewController.h"

@interface SamsungTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *samsungList;
@property (nonatomic, strong) NSArray *samsungKeys;
@property (nonatomic, strong) NSArray *samsungSongs;
@property (nonatomic, retain) AVAudioPlayer *welcomeTone;

@end
