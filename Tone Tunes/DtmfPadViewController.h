//
//  DtmfPadViewController.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tune.h"
#import "DtmfTableViewController.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioToolbox.h>


@interface DtmfPadViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) Tune *song;
@property (strong, nonatomic) IBOutlet UITextView *dsong;
@property (strong, nonatomic) IBOutlet UINavigationItem *dnavbar;

- (IBAction)upd1:(id)sender;
- (IBAction)dwd1:(id)sender;
- (IBAction)upd2:(id)sender;
- (IBAction)dwd2:(id)sender;
- (IBAction)upd3:(id)sender;
- (IBAction)dwd3:(id)sender;
- (IBAction)upd4:(id)sender;
- (IBAction)dwd4:(id)sender;
- (IBAction)upd5:(id)sender;
- (IBAction)dwd5:(id)sender;
- (IBAction)upd6:(id)sender;
- (IBAction)dwd6:(id)sender;
- (IBAction)upd7:(id)sender;
- (IBAction)dwd7:(id)sender;
- (IBAction)upd8:(id)sender;
- (IBAction)dwd8:(id)sender;
- (IBAction)upd9:(id)sender;
- (IBAction)dwd9:(id)sender;
- (IBAction)upds:(id)sender;
- (IBAction)dwds:(id)sender;
- (IBAction)upd0:(id)sender;
- (IBAction)dwd0:(id)sender;
- (IBAction)updp:(id)sender;
- (IBAction)dwdp:(id)sender;



-(void)initDtmf;
@property (nonatomic, retain) AVAudioPlayer *player0;
@property (nonatomic, retain) AVAudioPlayer *player1;
@property (nonatomic, retain) AVAudioPlayer *player2;
@property (nonatomic, retain) AVAudioPlayer *player3;
@property (nonatomic, retain) AVAudioPlayer *player4;
@property (nonatomic, retain) AVAudioPlayer *player5;
@property (nonatomic, retain) AVAudioPlayer *player6;
@property (nonatomic, retain) AVAudioPlayer *player7;
@property (nonatomic, retain) AVAudioPlayer *player8;
@property (nonatomic, retain) AVAudioPlayer *player9;
@property (nonatomic, retain) AVAudioPlayer *players;
@property (nonatomic, retain) AVAudioPlayer *playerp;

@end
