//
//  SamsungPadViewController.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <UIKit/UIKit.h>
#import "Tune.h"
#import "SamsungTableViewController.h"

@interface SamsungPadViewController : UIViewController


- (IBAction)s1:(id)sender;
- (IBAction)s2:(id)sender;
- (IBAction)s3:(id)sender;
- (IBAction)s4:(id)sender;
- (IBAction)s5:(id)sender;
- (IBAction)s6:(id)sender;
- (IBAction)s7:(id)sender;
- (IBAction)s8:(id)sender;
- (IBAction)s9:(id)sender;
- (IBAction)ss:(id)sender;
- (IBAction)s0:(id)sender;
- (IBAction)sp:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *ssong;
@property (strong, nonatomic) IBOutlet UINavigationItem *snavbar;

-(void)initPlayers;
@property (nonatomic, strong) AVAudioPlayer *player0;
@property (nonatomic, strong) AVAudioPlayer *player1;
@property (nonatomic, strong) AVAudioPlayer *player2;
@property (nonatomic, strong) AVAudioPlayer *player3;
@property (nonatomic, strong) AVAudioPlayer *player4;
@property (nonatomic, strong) AVAudioPlayer *player5;
@property (nonatomic, strong) AVAudioPlayer *player6;
@property (nonatomic, strong) AVAudioPlayer *player7;
@property (nonatomic, strong) AVAudioPlayer *player8;
@property (nonatomic, strong) AVAudioPlayer *player9;
@property (nonatomic, strong) AVAudioPlayer *players;
@property (nonatomic, strong) AVAudioPlayer *playerp;
@property (nonatomic, strong) AVAudioPlayer *hey;


@property (strong, nonatomic) Tune *song;

@end
