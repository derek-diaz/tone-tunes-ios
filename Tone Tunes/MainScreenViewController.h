//
//  MainScreenViewController.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "Tune.h"

@interface MainScreenViewController : UIViewController <ADBannerViewDelegate>
@property (strong, nonatomic) IBOutlet ADBannerView *ad;
@property(assign, nonatomic) BOOL bannerIsVisible;


@end
