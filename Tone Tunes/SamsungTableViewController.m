//
//  SamsungTableViewController.m
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import "SamsungTableViewController.h"

@interface SamsungTableViewController ()

@end

@implementation SamsungTableViewController
@synthesize samsungKeys, samsungList, samsungSongs, welcomeTone;


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    //Use this on tone tunes when selecting the different pads!
    if ([segue.identifier isEqualToString:@"showSamsungPad"]) {
        SamsungPadViewController *dvc = [segue destinationViewController];
        NSIndexPath *path = [self.tableView indexPathForSelectedRow]; //Get selected Index
        Tune *song = [[Tune alloc] init]; // Create song object
        
        //Set values
        song.title = [samsungKeys objectAtIndex:path.row];
        song.song = [samsungSongs objectAtIndex:path.row];
        
        //Send data to another screen!
        [dvc setSong:song];
        
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Song list directory
    NSString *songList = [[NSBundle mainBundle] pathForResource:@"samsung" ofType:@"plist"];
        
    //Load song list
    samsungList = [[NSDictionary alloc] initWithContentsOfFile:songList];
    
    //Load content
    samsungKeys = [samsungList allKeys];
    samsungSongs = [samsungList allValues];
    
    NSString *soundFilePath =[[NSBundle mainBundle] pathForResource: @"hey" ofType: @"caf"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    welcomeTone =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    
    [welcomeTone prepareToPlay];
    [welcomeTone play];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return samsungList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SamsungCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSString *currentSong;
    
    currentSong = [samsungKeys objectAtIndex:indexPath.row];
    [[cell textLabel] setText:currentSong];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
