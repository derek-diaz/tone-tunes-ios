//
//  SamsungPadViewController.m
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import "SamsungPadViewController.h"

@interface SamsungPadViewController ()

@end

@implementation SamsungPadViewController
@synthesize ssong;
@synthesize snavbar;
@synthesize song;
@synthesize player0, player1, player2, player3, player4, player5, hey,
            player6, player7, player8, player9, players, playerp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)initPlayers{
    
    NSString *soundFilePath =[[NSBundle mainBundle] pathForResource: @"a0" ofType: @"caf"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    AVAudioPlayer *newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player0 = newPlayer;
    
    [player0 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a1" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player1 = newPlayer;
    [player1 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a2" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player2 = newPlayer;
    [player2 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a3" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player3 = newPlayer;
    [player3 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a4" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player4 = newPlayer;
    [player4 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a5" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player5 = newPlayer;
    [player5 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a6" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player6 = newPlayer;
    [player6 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a7" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player7 = newPlayer;
    [player7 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a8" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player8 = newPlayer;
    [player8 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"a9" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    player9 = newPlayer;
    [player9 prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"as" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    players = newPlayer;
    [players prepareToPlay];
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"ap" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    playerp = newPlayer;
    [playerp prepareToPlay];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    NSString *convertedSong = [song.song stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    [ssong setText:convertedSong];
    [snavbar setTitle:song.title];
    [self initPlayers];
    
    if([song.title isEqualToString:@"Song of Time (Zelda)"]){
        
        NSString *soundFilePath =[[NSBundle mainBundle] pathForResource: @"hey_navi" ofType: @"caf"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
        hey =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
        [hey prepareToPlay];
        [hey play];
    
    }
    
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setSsong:nil];
    [self setSnavbar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)s1:(id)sender {
    if ([player1 isPlaying] == YES) {
        [player1 stop];
        [player1 setCurrentTime:0];
        [player1 prepareToPlay];
        [player1 play];

    }else{
        [player1 play];
    }

}

- (IBAction)s2:(id)sender {

    if ([player2 isPlaying] == YES) {
        [player2 stop];
        [player2 setCurrentTime:0];
        [player2 prepareToPlay];
        [player2 play];
        
    }else{
        [player2 play];
    }
}

- (IBAction)s3:(id)sender {

    if ([player3 isPlaying] == YES) {
        [player3 stop];
        [player3 setCurrentTime:0];
        [player3 prepareToPlay];
        [player3 play];
        
    }else{
        [player3 play];
    }
}

- (IBAction)s4:(id)sender {

    if ([player4 isPlaying] == YES) {
        [player4 stop];
        [player4 setCurrentTime:0];
        [player4 prepareToPlay];
        [player4 play];
        
    }else{
        [player4 play];
    }
}

- (IBAction)s5:(id)sender {

    if ([player5 isPlaying] == YES) {
        [player5 stop];
        [player5 setCurrentTime:0];
        [player5 prepareToPlay];
        [player5 play];
        
    }else{
        [player5 play];
    }
}

- (IBAction)s6:(id)sender {

    if ([player6 isPlaying] == YES) {
        [player6 stop];
        [player6 setCurrentTime:0];
        [player6 prepareToPlay];
        [player6 play];
        
    }else{
        [player6 play];
    }
}

- (IBAction)s7:(id)sender {
    
    if ([player7 isPlaying] == YES) {
        [player7 stop];
        [player7 setCurrentTime:0];
        [player7 prepareToPlay];
        [player7 play];
        
    }else{
        [player7 play];
    }
}

- (IBAction)s8:(id)sender {

    if ([player8 isPlaying] == YES) {
        [player8 stop];
        [player8 setCurrentTime:0];
        [player8 prepareToPlay];
        [player8 play];
        
    }else{
        [player8 play];
    }
}

- (IBAction)s9:(id)sender {

    if ([player9 isPlaying] == YES) {
        [player9 stop];
        [player9 setCurrentTime:0];
        [player9 prepareToPlay];
        [player9 play];
        
    }else{
        [player9 play];
    }
}

- (IBAction)ss:(id)sender {

    if ([players isPlaying] == YES) {
        [players stop];
        [players setCurrentTime:0];
        [players prepareToPlay];
        [players play];
        
    }else{
        [players play];
    }
}

- (IBAction)s0:(id)sender {

    if ([player0 isPlaying] == YES) {
        [player0 stop];
        [player0 setCurrentTime:0];
        [player0 prepareToPlay];
        [player0 play];
        
    }else{
        [player0 play];
    }
}

- (IBAction)sp:(id)sender {
    
    if ([playerp isPlaying] == YES) {
        [playerp stop];
        [playerp setCurrentTime:0];
        [playerp prepareToPlay];
        [playerp play];
        
    }else{
        [playerp play];
    }
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)playedSuccessfully
{
    //self.player = nil;
}
@end
