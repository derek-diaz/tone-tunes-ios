//
//  Tune.h
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tune : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic, strong) NSString *song;

@end
