//
//  MainScreenViewController.m
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import "MainScreenViewController.h"

@interface MainScreenViewController ()
    
@end

@implementation MainScreenViewController
@synthesize ad;
@synthesize bannerIsVisible;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    ad.delegate = self;
    
}



- (void)viewDidUnload
{
    [self setAd:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner{
    
    if (!self.bannerIsVisible)
    {
        NSLog(@"bannerViewDidLoadAd");
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGSize screenSize = screenBound.size;
        CGFloat screenHeight = screenSize.height;
        
        if (screenHeight == 1024.00) {
            NSLog(@"iPad found");
            banner.frame = CGRectOffset(banner.frame, 0, -65);
        }else if (screenHeight == 568.00){
            NSLog(@"iPhone Retina found");
            banner.frame = CGRectOffset(banner.frame, 0, -50);
        }else if (screenHeight == 480.00){
            NSLog(@"iPhone found");
            banner.frame = CGRectOffset(banner.frame, 0, -137);
        }
        
        
        
        [UIView commitAnimations];
        bannerIsVisible = YES;
    }

    
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    if (self.bannerIsVisible)
    {
        NSLog(@"bannerView:didFailToReceiveAdWithError:");
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		// assumes the banner view is at the top of the screen.
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGSize screenSize = screenBound.size;
        CGFloat screenHeight = screenSize.height;
        if (screenHeight == 1024.00) {
            NSLog(@"iPad found");
            banner.frame = CGRectOffset(banner.frame, 0, 65);
        }else if (screenHeight == 568.00){
            NSLog(@"iPhone Retina found");
            banner.frame = CGRectOffset(banner.frame, 0, 50);
        }else if (screenHeight == 480.00){
            NSLog(@"iPhone found");
            banner.frame = CGRectOffset(banner.frame, 0, 137);
        }
        [UIView commitAnimations];
		bannerIsVisible = NO;    }
}



@end
