//
//  DtmfTableViewController.m
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import "DtmfTableViewController.h"

@interface DtmfTableViewController ()

@end

@implementation DtmfTableViewController
@synthesize dtmfKeys, dtmfList, dtmfSongs;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    //Use this on tone tunes when selecting the different pads!
    if ([segue.identifier isEqualToString:@"showDtmfPad"]) {
        DtmfPadViewController *dvc = [segue destinationViewController];
        NSIndexPath *path = [self.tableView indexPathForSelectedRow]; //Get selected Index
        Tune *song = [[Tune alloc] init]; // Create song object
        
        //Set values
        song.title = [dtmfKeys objectAtIndex:path.row];
        song.song = [dtmfSongs objectAtIndex:path.row];
        
        //Send data to another screen!
        [dvc setSong:song];
        
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Song list directory
    NSString *songList = [[NSBundle mainBundle] pathForResource:@"dtmf" ofType:@"plist"];
    
    //Load song list
    dtmfList = [[NSDictionary alloc] initWithContentsOfFile:songList];
    
    //Load content
    dtmfKeys = [dtmfList allKeys];
    dtmfSongs = [dtmfList allValues];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return dtmfList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DtmfCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSString *currentSong;
    
    currentSong = [dtmfKeys objectAtIndex:indexPath.row];
    [[cell textLabel] setText:currentSong];
    
    return cell;

}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
