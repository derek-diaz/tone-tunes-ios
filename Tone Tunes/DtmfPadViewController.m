//
//  DtmfPadViewController.m
//  Tone Tunes
//
//  Created by Derek Diaz on 7/31/12.
//  Copyright (c) 2012 Derek Diaz. All rights reserved.
//

#import "DtmfPadViewController.h"

@interface DtmfPadViewController ()

@end

@implementation DtmfPadViewController
@synthesize song;
@synthesize dsong;
@synthesize dnavbar;
@synthesize player0, player1, player2, player3, player4, player5, player6, player7, player8, player9, players, playerp;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return NO;
}

-(void)initDtmf{

    
    NSString *soundFilePath =[[NSBundle mainBundle] pathForResource:@"d0" ofType:@"caf"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    AVAudioPlayer *newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player0 = newPlayer;
    [player0 prepareToPlay];
    player0.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d1" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player1 = newPlayer;
    [player1 prepareToPlay];
    player1.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d2" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player2 = newPlayer;
    [player2 prepareToPlay];
    player2.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d3" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player3 = newPlayer;
    [player3 prepareToPlay];
    player3.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d4" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player4 = newPlayer;
    [player4 prepareToPlay];
    player4.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d5" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player5 = newPlayer;
    [player5 prepareToPlay];
    player5.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d6" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player6 = newPlayer;
    [player6 prepareToPlay];
    player6.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d7" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player7 = newPlayer;
    [player7 prepareToPlay];
    player7.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d8" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player8 = newPlayer;
    [player8 prepareToPlay];
    player8.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"d9" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    player9 = newPlayer;
    [player9 prepareToPlay];
    player9.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"ds" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    players = newPlayer;
    [players prepareToPlay];
    players.currentTime = 0.001;
    
    //-----------
    soundFilePath =[[NSBundle mainBundle] pathForResource: @"dp" ofType: @"caf"];
    
    fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
    newPlayer.numberOfLoops = -1;
    playerp = newPlayer;
    [playerp prepareToPlay];
    playerp.currentTime = 0.001;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *convertedSong = [song.song stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    [dsong setText:convertedSong];
    [dnavbar setTitle:song.title];
    [self initDtmf];
}

- (void)viewDidUnload
{
    [self setDsong:nil];
    [self setDnavbar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)upd1:(id)sender {
   if (player1.isPlaying) {
       [player1 stop];
       player1.currentTime = 0.001;
    }
}

- (IBAction)dwd1:(id)sender {
    if (!player1.isPlaying) {
      [player1 play];
    }

}

- (IBAction)upd2:(id)sender {
    if (player2.isPlaying) {
        [player2 stop];
        player2.currentTime = 0.001;
    }
}

- (IBAction)dwd2:(id)sender {
    if (!player2.isPlaying) {
        [player2 play];
    }
}

- (IBAction)upd3:(id)sender {
    if (player3.isPlaying) {
        [player3 stop];
        player3.currentTime = 0.001;
    }
}

- (IBAction)dwd3:(id)sender {
    if (!player3.isPlaying) {
        [player3 play];
    }
}

- (IBAction)upd4:(id)sender {
    if (player4.isPlaying) {
        [player4 stop];
        player4.currentTime = 0.001;
    }
}

- (IBAction)dwd4:(id)sender {
    if (!player4.isPlaying) {
        [player4 play];
    }
}

- (IBAction)upd5:(id)sender {
    if (player5.isPlaying) {
        [player5 stop];
        player5.currentTime = 0.001;
    }
}

- (IBAction)dwd5:(id)sender {
    if (!player5.isPlaying) {
        [player5 play];
    }
}

- (IBAction)upd6:(id)sender {
    if (player6.isPlaying) {
        [player6 stop];
        player6.currentTime = 0.001;
    }
}

- (IBAction)dwd6:(id)sender {
    if (!player6.isPlaying) {
        [player6 play];
    }
}

- (IBAction)upd7:(id)sender {
    if (player7.isPlaying) {
        [player7 stop];
        player7.currentTime = 0.001;
    }
}

- (IBAction)dwd7:(id)sender {
    if (!player7.isPlaying) {
        [player7 play];
    }
}

- (IBAction)upd8:(id)sender {
    if (player8.isPlaying) {
        [player8 stop];
        player8.currentTime = 0.001;
    }
}

- (IBAction)dwd8:(id)sender {
    if (!player8.isPlaying) {
        [player8 play];
    }
}

- (IBAction)upd9:(id)sender {
    if (player9.isPlaying) {
        [player9 stop];
        player9.currentTime = 0.001;
    }
}

- (IBAction)dwd9:(id)sender {
    if (!player9.isPlaying) {
        [player9 play];
    }
}

- (IBAction)upds:(id)sender {
    if (players.isPlaying) {
        [players stop];
        players.currentTime = 0.001;
    }
}

- (IBAction)dwds:(id)sender {
    if (!players.isPlaying) {
        [players play];
    }
}

- (IBAction)upd0:(id)sender {
    if (player0.isPlaying) {
        [player0 stop];
        player0.currentTime = 0.001;
    }
}

- (IBAction)dwd0:(id)sender {
    if (!player0.isPlaying) {
        [player0 play];
    }
}

- (IBAction)updp:(id)sender {
    if (playerp.isPlaying) {
        [playerp stop];
        playerp.currentTime = 0.001;
    }
}

- (IBAction)dwdp:(id)sender {
    if (!playerp.isPlaying) {
        [playerp play];
    }
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)playedSuccessfully
{
    //self.player = nil;
}
@end
